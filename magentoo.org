# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: magentoo
#+subtitle: “Main file” for =magentoo= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+description: Gentoo dashboard for Emacs
#+property: header-args :tangle yes :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

~magentoo~ allows to perform some Gentoo administration tasks conveniently from Emacs.

* Overview
The main interface is the command ~magentoo-dashboard~ which is autoloaded.  Note that by default ~magentoo-dashboard~ uses TRAMP to get admin access.  Make sure you have sudo, doas or su root access conveniently available.  For example, =M-x magentoo-dashboard= always runs ~etc-update --preen~.

When you =M-x magentoo-dashboard=, you'll see the following buffer:
#+begin_example magentoo
This is localhost (Linux x86_64 5.10.52-gentoo-mykernel) 13:31:07

News items (2)
2021-09-24  busybox removal from system set
2021-09-29  Possible failure to preserve libraries
Config updates (3)
0000 /etc/hosts…
0000 /etc/resolv.conf…
0001 /etc/resolv.conf…
#+end_example

Ellipsis =…= indicates that the section is unfoldable.  Press =TAB= on it to unfold.

You can perform some Gentoo administration tasks from this buffer.

For config updates, you'll see the diff inside.  The following keybindings are available:
- =a=, when on line with the file name, applies the update as is
- =k=, when on line with the file name, drops the update
- =RET=, when on line with the file name, or in the diff, starts interactive merge by means of ~ediff~ session

News items section lists /unread/ news.  Pressing =RET= on individual news items reads them.  Pressing =RET= on the line “News items” reads all news.

* Suggested config
#+begin_example elisp
(use-package magentoo
  :commands (gentoo-dashboard)
  ;; If you use app-emacs/portage, you may want to
  ;; :after (portage)
  :init (defalias 'gentoo-dashboard 'magentoo-dashboard)
  :custom (magentoo-privileged-tramp-method "sudo"))
#+end_example

* Meta
** To install
Add the ebuild repository and install from there:
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater
emerge app-emacs/magentoo
#+end_src

** Notes for contiributors
If you have a local version of repository, compile with
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/emacs-magentoo"
#+end_example

This will link function definitions to your local Org files.

** Metadata
#+begin_src elisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: gentoo

#+end_src

* Loading
On load, ~magentoo-dashboard~ is made available.  That's it for now:
#+begin_src elisp :results none
(require 'magentoo-core)
#+end_src
