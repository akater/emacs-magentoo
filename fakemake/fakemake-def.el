;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'magentoo
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("magentoo-core" "magentoo")
  site-lisp-config-prefix "80"
  license "GPL-3")
